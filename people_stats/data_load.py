from datetime import date, datetime
from json import load
from string import ascii_lowercase, ascii_uppercase, digits, punctuation

from peewee import fn
from requests import get

from people_stats.models import Coordinates, Location, Login, Person, Timezone


class DataHandler:
    def __init__(self, is_test=False):
        persons_count = Person.select().count()
        if persons_count == 0 and is_test is False:
            # data = self.load_from_file("data/persons.json")
            data = self.load_from_api()
            for person in data:
                values = self.parse_json(person)
                self.save_to_database(values)

    def load_from_api(self):
        req = get("https://randomuser.me/api?results=5000")
        return req.json()["results"]

    def load_from_file(self, filepath):
        with open(filepath) as f:
            data = load(f)
        return data["results"]

    @staticmethod
    def parse_json(data):
        gender = data.get("gender")
        title = data["name"]["title"]
        first_name = data["name"]["first"]
        last_name = data["name"]["last"]

        street_number = data["location"]["street"]["number"]
        street_name = data["location"]["street"]["name"]

        city = data["location"]["city"]
        state = data["location"]["state"]
        country = data["location"]["country"]
        postcode = data["location"]["postcode"]

        latitude = data["location"]["coordinates"]["latitude"]
        longitude = data["location"]["coordinates"]["longitude"]

        tm_offset = data["location"]["timezone"]["offset"]
        tm_description = data["location"]["timezone"]["description"]

        email = data["email"]

        uuid = data["login"]["uuid"]
        username = data["login"]["username"]
        password = data["login"]["password"]
        salt = data["login"]["salt"]
        md5 = data["login"]["md5"]
        sha1 = data["login"]["sha1"]
        sha256 = data["login"]["sha256"]

        birthday = data["dob"]["date"]

        birthday_iso_compat = birthday.replace("Z", "+00:00")
        date_birthday = datetime.fromisoformat(birthday_iso_compat).date()
        today = date.today()
        today = today.replace(year=date_birthday.year)
        delta = date_birthday - today
        days_to_birthday = delta.days

        # add year if bday already occurred in current year
        if days_to_birthday < 0:
            days_to_birthday += 365

        registered_date = data["registered"]["date"]

        phone = "".join(c for c in data["phone"] if c.isdigit())
        cell = "".join(c for c in data["cell"] if c.isdigit())

        id_name = data["id"]["name"]
        id_value = data["id"]["value"]

        nat = data["nat"]

        return {
            "gender": gender,
            "title": title,
            "first_name": first_name,
            "last_name": last_name,
            "street_number": street_number,
            "street_name": street_name,
            "city": city,
            "state": state,
            "country": country,
            "postcode": postcode,
            "latitude": latitude,
            "longitude": longitude,
            "offset": tm_offset,
            "description": tm_description,
            "email": email,
            "uuid": uuid,
            "username": username,
            "password": password,
            "salt": salt,
            "md5": md5,
            "sha1": sha1,
            "sha256": sha256,
            "birthday": birthday,
            "days_to_birthday": days_to_birthday,
            "registered_date": registered_date,
            "phone": phone,
            "cell": cell,
            "id_name": id_name,
            "id_value": id_value,
            "nat": nat,
        }

    @staticmethod
    def save_to_database(data):
        coordinates = Coordinates.create(
            latitude=data["latitude"], longitude=data["longitude"]
        )
        timezone = Timezone.create(
            offset=data["offset"], description=data["description"]
        )
        location = Location.create(
            street_number=data["street_number"],
            street_name=data["street_name"],
            city=data["city"],
            state=data["state"],
            country=data["country"],
            postcode=data["postcode"],
            coordinates=coordinates,
            timezone=timezone,
        )
        login = Login.create(
            uuid=data["uuid"],
            username=data["username"],
            password=data["password"],
            salt=data["salt"],
            md5=data["md5"],
            sha1=data["sha1"],
            sha256=data["sha256"],
        )
        Person.insert(
            gender=data["gender"],
            title=data["title"],
            first_name=data["first_name"],
            last_name=data["last_name"],
            location=location,
            email=data["email"],
            login=login,
            birthday=data["birthday"],
            days_to_birthday=data["days_to_birthday"],
            registered_date=data["registered_date"],
            phone=data["phone"],
            cell=data["cell"],
            id_name=data["id_name"],
            id_value=data["id_value"],
            nat=data["nat"],
        ).execute()

    # data from database

    @staticmethod
    def gender_percentage():
        women_count = Person.select().where(Person.gender == "female").count()
        men_count = Person.select().where(Person.gender == "male").count()
        all_count = women_count + men_count

        return (women_count / all_count * 100, men_count / all_count * 100)

    @staticmethod
    def average_age(who):
        if who == "all":
            people = Person.select()
        elif who == "women":
            people = Person.select().where(Person.gender == "female")
        elif who == "men":
            people = Person.select().where(Person.gender == "male")
        age_sum = 0
        for person in people.iterator():
            birthday_iso_compat = person.birthday.replace("Z", "+00:00")
            date_birthday = datetime.fromisoformat(birthday_iso_compat).date()
            today = date.today()
            age_sum += round((today - date_birthday).days / 365, 2)

        return age_sum / people.count()

    @staticmethod
    def most_common_cities(n):
        locations = (
            Location.select(Location.city, fn.Count(Location.city).alias("count"))
            .group_by(Location.city)
            .order_by(fn.Count(Location.city).desc())
            .limit(n)
        )
        return [(location.city, location.count) for location in locations]

    @staticmethod
    def most_common_passwords(n):
        logins = (
            Login.select(Login.password, fn.Count(Login.password).alias("count"))
            .group_by(Login.password)
            .order_by(fn.Count(Login.password).desc())
            .limit(n)
        )
        return [(login.password, login.count) for login in logins]

    @staticmethod
    def born_in_time_period(since_string, to_string):
        try:
            since = datetime.strptime(since_string, "%Y-%m-%d")
            to = datetime.strptime(to_string, "%Y-%m-%d")
        except ValueError:
            print("Date argument doesn't match format '%Y-%m-%d'")
            return []
        born_between_dates = Person.select().where(
            Person.birthday > since, Person.birthday < to
        )
        return [
            (
                person.title,
                person.first_name,
                person.last_name,
                person.email,
                person.birthday,
            )
            for person in born_between_dates
        ]

    @staticmethod
    def score_password(password):
        """Not efficient but good readability"""
        score = 0
        if any(char in ascii_lowercase for char in password):
            score += 1
        if any(char in ascii_uppercase for char in password):
            score += 2
        if any(char in digits for char in password):
            score += 1
        if len(password) >= 8:
            score += 5
        if any(char in punctuation for char in password):
            score += 3

        return score

    def most_secure_passwords(self, n):
        logins = Login.select(Login.password).group_by(Login.password)
        data = [
            (self.score_password(login.password), login.password) for login in logins
        ]
        return sorted(data, key=lambda t: t[0], reverse=True)[:n]
