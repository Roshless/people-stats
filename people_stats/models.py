from peewee import (CharField, DateField, ForeignKeyField, IntegerField, Model,
                    SqliteDatabase, UUIDField)

db = SqliteDatabase("people.db")


class BaseModel(Model):
    class Meta:
        database = db


class Coordinates(BaseModel):
    latitude = IntegerField()
    longitude = IntegerField()


class Timezone(BaseModel):
    offset = CharField()
    description = CharField()


class Location(BaseModel):
    street_number = IntegerField()
    street_name = CharField()

    city = CharField()
    state = CharField()
    country = CharField()
    postcode = CharField()

    coordinates = ForeignKeyField(Coordinates)
    timezone = ForeignKeyField(Timezone)


class Login(BaseModel):
    uuid = UUIDField(unique=True)
    username = CharField()
    password = CharField()
    salt = CharField()
    md5 = CharField()
    sha1 = CharField()
    sha256 = CharField()


class Person(BaseModel):
    gender = CharField(default="Undefined")

    title = CharField()
    first_name = CharField()
    last_name = CharField()

    location = ForeignKeyField(Location)

    email = CharField()
    login = ForeignKeyField(Login)

    birthday = DateField()
    days_to_birthday = IntegerField()
    registered_date = DateField()

    phone = IntegerField()
    cell = IntegerField()

    id_name = CharField(null=True)
    id_value = CharField(null=True)

    nat = CharField()


MODELS = (Coordinates, Timezone, Location, Login, Person)

db.connect()
db.create_tables(MODELS)
