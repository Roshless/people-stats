from argparse import ArgumentParser
from datetime import datetime
from sys import exit, stderr

from people_stats.data_load import DataHandler


class PeopleStatsCLI:
    def create_parser(self):
        parser = ArgumentParser(
            prog="people-stats", description="Statistics about people."
        )
        parser.add_argument(
            "--gender-percentage", help="show gender proportions", action="store_true",
        )
        parser.add_argument(
            "--average-age",
            metavar="all, women, men",
            type=str,
            choices=["all", "women", "men"],
            nargs="?",
            const="all",
            help="show average age",
        )
        parser.add_argument(
            "--most-common-cities",
            metavar="N",
            type=int,
            help="show N most popular cities",
        )
        parser.add_argument(
            "--most-common-passwords",
            metavar="N",
            type=int,
            help="show N most popular user passwords",
        )
        parser.add_argument(
            "--born-in-time-period",
            metavar="YYYY-MM-DD",
            nargs=2,
            type=str,
            help="show people born between 2 dates",
        )
        parser.add_argument(
            "--most-secure-passwords",
            metavar="N",
            type=int,
            help="show N most secure passwords",
        )

        return parser

    def run(self):
        dt = DataHandler()

        parser = self.create_parser()
        args = parser.parse_args()

        if args.gender_percentage:
            women_per, men_per = dt.gender_percentage()
            print(f"Women: {women_per:.2f}%")
            print(f"Men: {men_per:.2f}%")
        elif opt := args.average_age:
            print(f"Average age of {opt}: {dt.average_age(opt):.2f} years")
        elif n := args.most_common_cities:
            cities = dt.most_common_cities(n)
            for city, occurrences in cities:
                print(f"{occurrences} - {city}")
        elif n := args.most_common_passwords:
            passwords = dt.most_common_passwords(n)
            for password, occurrences in passwords:
                print(f"{occurrences} - {password}")
        elif dates := args.born_in_time_period:
            for title, first_name, last_name, email, birthday in dt.born_in_time_period(
                dates[0], dates[1]
            ):
                birthday_iso_compat = birthday.replace("Z", "+00:00")
                date_birthday = datetime.fromisoformat(birthday_iso_compat).strftime(
                    "%Y-%m-%d"
                )
                print(
                    f"{title} {first_name} {last_name} - {email} - born:{date_birthday}"
                )
        elif n := args.most_secure_passwords:
            top = dt.most_secure_passwords(n)
            for score, password in top:
                print(f"{password} - {score} points")
        else:
            parser.print_help()
            print("\nChoose one operation", file=stderr)
            exit(1)
