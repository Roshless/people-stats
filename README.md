# people_stats 
### lots of data about specific persons 


```
usage: people-stats [-h] [--gender-percentage] [--average-age [all, women, men]] [--most-common-cities N]
                    [--most-common-passwords N] [--born-in-time-period YYYY-MM-DD YYYY-MM-DD] [--most-secure-passwords N]

Statistics about people.

optional arguments:
  -h, --help            show this help message and exit
  --gender-percentage   show gender proportions
  --average-age [all, women, men]
                        show average age
  --most-common-cities N
                        show N most popular cities
  --most-common-passwords N
                        show N most popular user passwords
  --born-in-time-period YYYY-MM-DD YYYY-MM-DD
                        show people born between 2 dates
  --most-secure-passwords N
                        show N most secure passwords
```


# Run

Create virtual environment:

`virtualenv venv` 


Switch to it:

`source venv/bin/activate`


Install dependencies:

`pip install -r requirements.txt`


Run with:

`python -m people_stats`


# Tests

Run with:

`python -m pytest`
